import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import CustomPlugin from './plugins/plugin'

Vue.use(CustomPlugin, {
  type: 'install',
  date: new Date().getTime()
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
