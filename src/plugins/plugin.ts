import Vue from 'vue'

class Plugin {
  private static instance: Plugin | null = null;
  private constructor(public opts?: any) {
    console.log('constructing plugin')
    console.log(opts)
    console.log('end constructing plugin')
  }

  static getInstance(opts?: any): Plugin {
    if(!Plugin.instance) {
      Plugin.instance = new Plugin(opts)
    }
    return Plugin.instance
  }

  getParam() {
    return this.opts
  }
}

export default {
  install(vue: typeof Vue, opts: any) {
    console.log('[custom-plugin] - install', {
      opts
    })
    Vue.prototype.$plug = Plugin.getInstance(opts)
  },
  get(opts?: any) {
    console.log('[custom-plugin] - get', {
      opts
    })
    return Plugin.getInstance()
  }
}

declare module 'vue/types/vue' {
  interface Vue {
    $plug: Plugin
  }
}
