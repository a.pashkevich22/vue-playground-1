import Vue from 'vue'
import Vuex from 'vuex'
import MainService from "@/services/main";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    param: '',
  },
  mutations: {
    setParam(state, param) {
      console.log('setParam', {
        param
      })
      state.param = param
    }
  },
  actions: {
    getParam({ commit }) {
      const data = MainService.getParam()
      commit('setParam', data)
    }
  },
  modules: {
  }
})
