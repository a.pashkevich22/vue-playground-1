import CustomPlugin from '@/plugins/plugin'

const $plugin = CustomPlugin.get()

export default class MainService {
  static getParam() {
    return $plugin.getParam()
  }
}
